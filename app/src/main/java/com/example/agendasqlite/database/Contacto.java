package com.example.agendasqlite.database;

import java.io.Serializable;

public class Contacto implements Serializable {
    private  long  _ID;
    private String nombre;
    private String domicilio;
    private int favorito;
    private String telefono1;
    private String telefono2;
    private String notas;

    public Contacto(long _ID, String nombre, String telefono1, String telefono2, String domicilio, String notas, int favorito ) {
        this._ID = _ID;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.favorito = favorito;
        this.telefono1 = telefono1;
        this.telefono2 = telefono2;
        this.notas = notas;
    }

    public Contacto() {
    }

    public long get_ID() {
        return _ID;
    }

    public void set_ID(long _ID) {
        this._ID = _ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getFavorito() {
        return favorito;
    }

    public void setFavorito(int favorito) {
        this.favorito = favorito;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }


}
